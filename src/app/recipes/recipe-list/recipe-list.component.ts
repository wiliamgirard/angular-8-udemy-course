import { Component, OnInit } from '@angular/core';
import { Recipe } from '../recipes.model';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
  recipes: Recipe[] = [
    new Recipe('A test recipe', 'a test men', 'https://p2.trrsf.com/image/fget/cf/940/0/images.' +
      'terra.com/2019/05/29/receitas-de-omelete.jpg'),
    new Recipe('Another test recipe', 'another test men', 'https://www.gazetadopovo.com.br/' +
      'bomgourmet/wp-content/uploads/2019/06/chuchu-assado1-670x430.jpg')
  ];

  constructor() { }

  ngOnInit() {
  }
}
